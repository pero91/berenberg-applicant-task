<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Header\SetCookie;
use Application\Form\AppForm;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * This action displays the "Test App Form" page.
     */
    public function appformAction() 
    {
        // Create "Test App" form
        $form = new AppForm();
        
        // Check if user has submitted the form
        if($this->getRequest()->isPost()) {
            
            $data = $this->params()->fromPost();
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                $data = $form->getData();
                $email = $data['email'];
                $firstname = $data['firstname'];
                $lastname = $data['lastname'];

                $cookie = new SetCookie('firstname', $firstname, time()+7200);
                $this->getResponse()->getHeaders()->addHeader($cookie);
                
                // Redirect to "Thank You" page
                return $this->redirect()->toRoute('application',
                        ['action'=>'thankYou']);
            }
        }
        
        // Pass form variable to view
        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action displays the Thank You page. The user is redirected to this
     * page on successful mail delivery.
     */
    public function thankYouAction() 
    {
        return new ViewModel();
    }
    
    /**
     * This action displays the Send Error page. The user is redirected to this
     * page on mail delivery error.
     */
    public function errorAction() 
    {
        return new ViewModel();
    }
}
