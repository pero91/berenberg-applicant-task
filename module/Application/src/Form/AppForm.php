<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

// Form Class for Berenberg Applicant test
// @author Petar Simic
class AppForm extends Form
{
    public function __construct()
    {
        // define a name for this form
        parent::__construct('appform');

        // Add fields for this form
        $this->addElements();

        // Add validation for this form
        $this->addInputFilter(); 
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'name' => 'firstname',
            'options' => [
                'label' => 'First name',
            ],
            'type'  => 'Text',
        ]);
        $this->get('firstname')->setLabelAttributes(['class' => 'col-sm-3 col-form-label']);
        $this->add([
            'name' => 'lastname',
            'options' => [
                'label' => 'Last name',
            ],
            'type'  => 'Text',
        ]);
        $this->get('lastname')->setLabelAttributes(['class' => 'col-sm-3 col-form-label']);
        $this->add([
            'name' => 'email',
            'options' => [
                'label' => 'Email address',
            ],
            'type' => Element\Email::class,
        ]);
        $this->get('email')->setLabelAttributes(['class' => 'col-sm-3 col-form-label']);
        // Add "Send" button
        $this->add([
            'name' => 'send',
            'type'  => 'Submit',
            'attributes' => [
                'value' => 'Send',
            ],
        ]);
    }

    private function addInputFilter() 
    {
        $inputFilter = $this->getInputFilter();
        
        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],                
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                    ],
                ],
            ],
        ]);
    }
}